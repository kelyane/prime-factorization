#include <stdio.h>

int main(void)
{
       int n, p, q, primo, div;
       printf("Informe n: ");
       scanf("%d", &n);
       p = 2;
	   
	   printf("Fatoracao de %d:", n);
       while (n > 1) {
		   
             while (n % p == 0) {               
                n = n / p;
				printf ("%d " , p);
			 }
             
              primo = 0;
              while (! primo) {
                       p = p + 1;
                       div = 2;
                      primo = 1;
                  while (div <= p / 2 && primo) {
                       if (p % div == 0)
                            primo = 0;
                       else
                            div = div + 1;
                 }
         }
     }
     printf ("\n");
   return 0;
}