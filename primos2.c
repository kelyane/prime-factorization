#include<stdio.h>
#include<stdlib.h>
typedef unsigned long long int  ullint;

void fatoracaoPrimos(ullint n){
		
    while(n%2 == 0){
        printf("%d ",2);
        n = n/2;
    }
 	
 	ullint i = 3;
	
	while((i*i) < n){ 
    
        while(n%i == 0){
            printf("%lld ",i);
            n = n/i;
        }
        
        i = i+2;
    }

    if (n > 2){
        printf ("%lld ",n);
	}
}
 
int main(int argc, char **argv){
	
	char *end;
	
    ullint n = strtoull(argv[1], &end, 10);
	printf("%lld \n",n);
	
    fatoracaoPrimos(n);
	
	printf ("\n");
    return 0;
}

