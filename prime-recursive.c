#include<stdio.h>
#include<stdlib.h>
#include<math.h>
typedef unsigned long long int  ullint;

ullint geradordivisor(ullint n, ullint f){
	
	ullint x = sqrt(n);
	
	while(n%f!=0 && f<x){		
		f += f == 2 ? 1 : 2;
	}
		
	return n%f == 0 ? f : n;
}

void fatoracaoPrimos(ullint n, ullint f){
	
	if(n==1){
		return;
	}
	
	ullint divisor = geradordivisor(n,f);
	printf("%lld \n",divisor);
	
	
	fatoracaoPrimos(n/divisor, divisor);
}

int main(int argc, char **argv){
    ullint n = atoll(argv[1]);
	printf ("%lld \n", n);
	
    fatoracaoPrimos(n,2);
	
	printf ("\n");
    return 0;
}
